#!/bin/bash

WHEREAMI=`pwd`

# $HYBRIDD/$NODE/scripts/npm  => $HYBRIDD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"
DETERMINISTIC="$HYBRIDD/deterministic"
NODEJS="$HYBRIXD/nodejs"
COMMON="$HYBRIXD/common"
INTERFACE="$HYBRIXD/hybrix-lib"
WEB_BLOCKEXPLORER="$HYBRIXD/web-explorer"

echo "[i] Copy files to dist."
rsync -aK "$WEB_BLOCKEXPLORER/lib/" "$WEB_BLOCKEXPLORER/dist/"


while read LINE; do

    echo "$LINE" > "$WEB_BLOCKEXPLORER/dist/tmp"
    SYMBOL=$(cut -d " " -f 1 "$WEB_BLOCKEXPLORER/dist/tmp")
    ALIAS=$(cut -d " " -f 2 "$WEB_BLOCKEXPLORER/dist/tmp")
    echo "[.] Create alias $ALIAS -> $SYMBOL"

    cp "$WEB_BLOCKEXPLORER/dist/files/index.html" "$WEB_BLOCKEXPLORER/dist/files/$ALIAS"

    sed 's/input id\=\"symbol\"/input id\=\"symbol\" value\=\"'$SYMBOL'\"/g' "$WEB_BLOCKEXPLORER/dist/files/$ALIAS" > "$WEB_BLOCKEXPLORER/dist/files/$ALIAS.tmp"

    mv "$WEB_BLOCKEXPLORER/dist/files/$ALIAS.tmp" "$WEB_BLOCKEXPLORER/dist/files/$ALIAS"

done < "$WEB_BLOCKEXPLORER/lib/alias"

echo "[i] Copy files to node."
mkdir -p "$NODE/modules/web-blockexplorer/files"
rsync -aK "$WEB_BLOCKEXPLORER/dist/" "$NODE/modules/web-blockexplorer/"

cd "$WHEREAMI"
echo "[.] All done."
