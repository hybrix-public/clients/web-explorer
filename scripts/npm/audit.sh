#!/bin/sh
WHEREAMI="`pwd`";
OLDPATH="$PATH"

# $WEB_EXPLORER/scripts/npm  => $WEB_EXPLORER
SCRIPTDIR="`dirname \"$0\"`"
WEB_EXPLORER="`cd \"$SCRIPTDIR/../..\" && pwd`"

export PATH="$WEB_EXPLORER/node_binaries/bin:$PATH"

cd "$WEB_EXPLORER"
echo "[.] Auditing web-blockexplorer..."
npm i
npm update
npm audit fix --force

export PATH="$OLDPATH"
cd "$WHEREAMI"
