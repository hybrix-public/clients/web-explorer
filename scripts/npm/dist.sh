#!/bin/sh

WHEREAMI=`pwd`

# $HYBRIXD/$NODE/scripts/npm  => $HYBRIXD
SCRIPTDIR="`dirname \"$0\"`"
HYBRIXD="`cd \"$SCRIPTDIR/../../..\" && pwd`"

NODE="$HYBRIXD/node"
DETERMINISTIC="$HYBRIXD/deterministic"
NODEJS="$HYBRIXD/nodejs-v8-lts"
COMMON="$HYBRIXD/common"
INTERFACE="$HYBRIXD/interface"
WEB_BLOCKEXPLORER="$HYBRIXD/web-wallet"

echo "[i] Copy web-blockexplorer distributables to node."
mkdir -p "$NODE/modules/web-blockexplorer/files"
rsync -aK "$WEB_BLOCKEXPLORER/dist/" "$NODE/modules/web-blockexplorer/"

cd "$WHEREAMI"
echo "[.] All done."
